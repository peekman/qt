package com.qt.api.exception;

public class FileTypeNotSupportedException extends RuntimeException {

    public FileTypeNotSupportedException(String file) {
        super("Üles on võimalik laadida õige struktuuriga JSON-fail. Üles laetud faili tüüp ei ole toetatud: " + file);
    }

}
