package com.qt.api.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qt.api.exception.FileTypeNotSupportedException;
import com.qt.api.model.Root;
import com.qt.api.model.Value;
import com.qt.api.service.RootServiceImpl;
import com.qt.api.service.ValueServiceImpl;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

@RestController
public class FileUploadController {
    public RootServiceImpl rootService;
    public ValueServiceImpl valueService;

    @Autowired
    public FileUploadController (RootServiceImpl rootService, ValueServiceImpl valueService) {
        this.rootService = rootService;
        this.valueService = valueService;
    }

    @RequestMapping(value = "/set_values", method = RequestMethod.POST, consumes = { "multipart/form-data" })
    public void upload(@RequestPart("file") @NotNull MultipartFile file) throws FileNotFoundException {
        fileInfo(file);
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<Value>> typeReference = new TypeReference<List<Value>>(){};
        try {
            List<Value> values = mapper.readValue(file.getBytes(), typeReference);
            rootService.deleteAll();
            valueService.deleteAll();
            for (Value v:values) {
                Root r = new Root(v);
                Value.statroot = r;
                rootService.save(r);
            }
            System.out.println("Values Saved!");
        } catch (IOException e){
            System.out.println("Unable to save values: " + e.getMessage());
            throw new FileTypeNotSupportedException(file.getOriginalFilename());
        }
    }

    private void fileInfo(MultipartFile file) {
        System.out.println("Uploaded File: ");
        System.out.println("Name : " + file.getName());
        System.out.println("Type : " + file.getContentType());
        System.out.println("Name : " + file.getOriginalFilename());
        System.out.println("Size : " + file.getSize());
    }
}
