package com.qt.api.controller;

import com.qt.api.model.Root;
import com.qt.api.service.RootServiceImpl;
import com.qt.api.service.ValueServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class apiController {
    public RootServiceImpl rootService;

    @Autowired
    public apiController(RootServiceImpl rootService) {
            this.rootService = rootService;
        }

    @GetMapping(path="/get_values_total")
    public List<Root> getValues()
        {
            return rootService.getRoots();
        }

}
