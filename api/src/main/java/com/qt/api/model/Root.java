package com.qt.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="roots")
@Getter
@Setter
@NoArgsConstructor
public class Root implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    private int total = 0;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="childs_id")
    public Value childs;

    @OneToMany(cascade = CascadeType.ALL)
    @JsonIgnore
    public List<Value> children = new ArrayList<>();

    public Root(Value v) {
        this.childs = v;
    }

    @PostPersist
    private void sumByRoot() {
        for (Value child : this.children) {
            this.total += child.getValue();
        }
    }

    public void addChild(Value v) {
        this.children.add(v);
    }

}

