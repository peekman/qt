package com.qt.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "values")
@Getter
@Setter
@NoArgsConstructor
public class Value implements Serializable {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "value", nullable = false)
    private int value;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="parent")
    private List<Value> childs = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JsonIgnore
    @Transient
    public static Root statroot;

    @ManyToOne(cascade = CascadeType.ALL)
    @Transient
    @JsonIgnore
    public Root root;

    @PrePersist
    // Üritasin leida lahendust, kus ei pea kasutama rekursiooni, et valuesid kokku liita.
    // Tegin lahenduse, kus kasutan ära Java class staatilist välja.
    // Faili sisu sisselugemisel saan salvestamist korraldada juurtippude (st siis esimese tasandi
    // tippude kaudu). Teen Root isendi ja seon ta nö flat kõikide ta alluvatega ja arvutan kohe ka
    // total väärtuse, sest see on endpointiga seotud, seega rohkem kasutatav ja mõistlik, et iga kord
    // ei arvutata välja, vaid hoitakse juba arvutatuna andmebaasis.
    public void takeRoot() {
        Value.statroot.addChild(this);
    }

    @Override
    public String toString(){
        return "Value ID: "+ this.id+ "; Value: " + this.value;
    }
}
