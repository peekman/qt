package com.qt.api.service;

import com.qt.api.model.Root;
import com.qt.api.repository.RootRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RootServiceImpl implements RootService {
    RootRepository rootRepository;

    @Autowired
    public RootServiceImpl(RootRepository rootRepository) {
        this.rootRepository = rootRepository;
    }

    public List<Root> getRoots() {
        return rootRepository.findAll();
    }

    @Override
    public Root save (Root root) {
        rootRepository.save(root);
        return root;
    }
    @Override
    public void deleteAll () {
        if (rootRepository.count() > 0) {
            rootRepository.deleteAll();
        }
    }

}
