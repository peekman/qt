package com.qt.api.service;

import com.qt.api.model.Value;
import com.qt.api.repository.ValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ValueServiceImpl implements ValueService {

    ValueRepository valueRepository;

    @Autowired
    public ValueServiceImpl (ValueRepository valueRepository) {
        this.valueRepository = valueRepository;
    }

    @Override
    public List<Value> getAllValues() {
        return valueRepository.findAll();
    }

    @Override
    public Value addValue(Value value) {
        valueRepository.save(value);
        return value;
    }

    @Override
    public void deleteAll() {
        if (valueRepository.count() > 0) {
            valueRepository.deleteAll();
        }
    }

    public Iterable<Value> saveAll (List<Value> values) {
        return valueRepository.saveAll(values);
    }

}

