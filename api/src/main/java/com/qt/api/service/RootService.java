package com.qt.api.service;

import com.qt.api.model.Root;
import org.springframework.stereotype.Service;

@Service
public interface RootService {
    Root save (Root root);
    void deleteAll ();
}
