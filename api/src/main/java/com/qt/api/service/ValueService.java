package com.qt.api.service;

import com.qt.api.model.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ValueService {

    List<Value> getAllValues();
    Value addValue(Value value);
    void deleteAll();

}