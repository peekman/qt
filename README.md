# Qt API



## Alustamine

API käivitamiseks peab arvutisse olema installitud docker, docker-compose, git. 

Kood asub avalikus repos ja selle saab kloonida aadressilt: https://gitlab.com/peekman/qt

## API kirjeldus

API abil saab andmebaasi sisse lugeda JSON-failist Value isendid, mille väljadeks on "value" ja "childs". Value isend saab olla iseenda alluv loendis "childs". Samuti saab API abil andmebaasi teha päringu ja vaadata andmebaasi laaditud valuesid. Väljundile lisatakse kõikide ühes puuharus olevate value väljade summa. 

Faili uuel laadimisel eemaldatakse andmebaasis eelnevalt laaditud andmed ja laetakse uued andmed.

APIl on kaks endpointi:
1) GET /get_values_total, mille kaudu saab vaadata andmebaasi laetud andmeid koos samas harus olevate value väljade summaga iga juurvalue lõikes.

2) POST /set_values, mille abil saab andmed lugeda andmebaas. Endpoint võtab vastu multipart requesti võtmega "file", mille juurde on lisatud õige struktuuriga JSON-faile.

Otse brauserist faili saatmise ja andmete laadimise kontrollimiseks on avalehel faili laadimisvorm, mis saadab faili /set_values endpointi. 

Testimiseks on reposse lisatud 3 faili: val3.json, val4.json ja values.json.

Olulised lingid, kui rakendus töötab localhostis:
1) API avaleht faili laadimise vormiga: http://localhost:8080/
2) andmebaasi päring: http://localhost:8080/get_values_total

## API komponendid

API on openjdk 11-l töötav Spring Boot RESTful rakendus. APIs on kasutatud teekidena:
Spring Boot Starter Web
Spring Boot Starter Data JPA
Liquibase
PostgreSQL
Lombok
Jackson Databind

Rakendus on kompileeritud jar-failiks ning paigutatud konteinerisse. Seega on rakenduse käivitamiseks vaja ainult installeerida docker ja docker-compose.

Lisaks on võimalik kasutada andmebaasi seisundi vaatamise Adminerit: http://localhost:7553

Kasutusel on PostgreSQL default user ja parool, andmebaasi nimi on "qt".

## Rakenduse installeerimine ja käivitamine

1. Klooni repo arvutisse: https://gitlab.com/peekman/qt.git

Peakasutas "qt" on:
- rakenduse kaust "api", kus on kogu vajalik koodi
- db kaust, mis ühendatakse dockeri konteineriga ja kus on skript qt-nimelise andmebaasi loomiseks. On loodud ka eraldi kasutaja "qt", kuid API defauldina kasutab PostgreSQL default kasutajat ja parool.
- jar-fail, mis on siia tõstetud asukohast: /qt/api/target
- Dockerfile API konteineriseerimiseks
- docker-compose.yml konteinerite käivitamiseks ja kus on kogu vajalik info parameetritega
- 3 õige struktuuriga JSON-faili testimiseks

2. Mine kausta "qt" ja ava Git Bash selles kaustas. (Windowsis peab taustal töötama Docker Desktop).
3. Sisesta Git Bashi käsk: docker image build -t qt-api .
4. Sisesta Git Bashi käsk: docker-compose up

Pärast rakenduse käivitamist peaks olema API kasutatav localhostis (http://localhost:8080/, http://localhost:8080/get_values_total)

Rakenduse käivitamist on testitud W11 keskkonas. Seal peab Docker Desktop töötama, et dockerit kasutada.

Kui on vaja koodi pealt uus jar-fail kompileerida, siis tuleks kasutada käsku "mvn package -DskipTests", kuna W11 operatsioonisüsteemis ei saa API konteineris oleva andmebaasiga ühendust ja tekib error.



