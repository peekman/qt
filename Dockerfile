FROM openjdk:11

COPY api-0.0.1-SNAPSHOT.jar /deployments/

CMD java -jar /deployments/api-0.0.1-SNAPSHOT.jar